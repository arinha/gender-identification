import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.ensemble.forest import RandomForestClassifier



f=20
voices=pd.read_csv('voice.csv')
#s stores the total size of the dataset
s=voices.shape

#this separates the predictive label
label=np.array(voices['label'])
voices=voices.drop('label', axis=1)
columnList=list(voices.columns)
voices=np.array(voices)


def pickleDumper (n=7, ram=54):
    import pickle
    #t1 and t2 help in storing the amount of dataset we will be using for the training purpose
    t2=(s[0]*(n-1)//(n*f))*f
    ratio=1-(t2/s[0]);


    #converting to numpy array
    train_features, test_features, train_labels, test_labels=train_test_split(voices, label, test_size=ratio, random_state=42)

    print('Training Features Shape:', train_features.shape)
    print('Training Labels Shape:', train_labels.shape)
    print('Testing Features Shape:', test_features.shape)
    print('Testing Labels Shape:', test_labels.shape)
    #making decision trees
    rf=RandomForestClassifier(n_estimators=ram, random_state=42)
    rf.fit(train_features, train_labels)
    predictions=rf.predict(test_features)
    print('Mean absolute accuracy: ', rf.score(test_features, test_labels))
    pickle._dump(rf, open('decision_trees.sav', 'wb'))

def foldPrecisionPlotter (folds, ram):
    import matplotlib.pyplot as pylot
    #s stores the total size of the dataset
    #t1 and t2 help in storing the amount of dataset we will be using for the training purpose
    #t1 and t2 help in storing the amount of dataset we will be using for the training purpose
    t2=(s[0]*(folds-1)//(folds*f))*f
    ratio=1-(t2/s[0]);
    
    voices=np.array(voices)
    train_features, test_features, train_labels, test_labels=train_test_split(voices, label, test_size=ratio, random_state=42)

    y=[]
    for i in range(1,ram):
        rf=RandomForestClassifier(n_estimators=i, random_state=42)
        rf.fit(train_features, train_labels)
        y.append(100*rf.score(test_features, test_labels))
    pylot.plot(range(1,ram), y)
    pylot.xlabel('Number of Decision Trees')
    pylot.ylabel('Accuracy (%)')
    pylot.show()
