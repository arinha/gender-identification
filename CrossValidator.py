import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.ensemble.forest import RandomForestClassifier
from sklearn.model_selection._validation import cross_val_score

n=10
f=20
voices=pd.read_csv('voice.csv')
#s stores the total size of the dataset
#t1 and t2 help in storing the amount of dataset we will be using for the training purpose
s=voices.shape
t2=(s[0]*(n-1)//(n*f))*f
ratio=1-(t2/s[0]);

#this separates the predictive label
label=np.array(voices['label'])
voices=voices.drop('label', axis=1)

#converting to numpy array
voices=np.array(voices)
train_features, test_features, train_labels, test_labels=train_test_split(voices, label, test_size=ratio, random_state=42)

print('Training Features Shape:', train_features.shape)
print('Training Labels Shape:', train_labels.shape)
print('Testing Features Shape:', test_features.shape)
print('Testing Labels Shape:', test_labels.shape)

y=[]
ram=60
rf=RandomForestClassifier(n_estimators=ram, random_state=42)
rf.fit(train_features, train_labels)
#predictions=rf.predict(test_features)
scores = cross_val_score(rf, voices, label, cv=n)
print('Acccuracy score: ', scores)
print ('Average score: ', sum(scores)/len(scores))